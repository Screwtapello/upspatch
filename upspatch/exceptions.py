"""
Exceptions raised while reading or applying UPS patches.
"""
# For copyright and licensing information, see the file COPYING.

class UPSException(Exception):
	pass

class PatchFormatException(UPSException):
	pass
