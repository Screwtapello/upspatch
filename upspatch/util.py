"""
Utility methods used when reading UPS patches.
"""
# For copyright and licensing information, see the file COPYING.
from os import SEEK_END
from struct import unpack
from mmap import mmap, ACCESS_READ
from array import array
from binascii import crc32
from upspatch.exceptions import UPSException, PatchFormatException

class CRCTrackingFileReader(object):
	"""
	File handle that calculates a CRC as bytes are read.

	Also provides methods that are useful for implementing UPS patching.
	"""

	def __init__(self, filename):
		self.handle = open(filename, 'rb')
		self.crc = 0

		# Since we never seek after we've opened the file, we can make things
		# faster by keeping track of the offset ourselves instead of calling
		# .tell()
		self.offset = 0

		# It's useful to know how long the file is.
		self.handle.seek(0, SEEK_END)
		self.size = self.handle.tell()
		self.handle.seek(self.offset)

		self.mmap = mmap(self.handle.fileno(), self.size, access=ACCESS_READ)

	def count_bytes_till_null(self):
		"""
		Returns the number of bytes from the current pos until the next NUL.

		Reading this many bytes should get you a string that ends with the NUL.
		"""
		res = self.mmap.find("\0", self.offset)
		if res == -1:
			raise PatchFormatException("Unexpected end-of-file while "
					"looking for a NUL byte after byte %d of %r" %
					(self.offset, self.handle.name))
		return res - self.offset + 1

	def read_bytes(self, count):
		"""
		Read an arbitrary number of bytes from the file.
		"""
		res = self.mmap[self.offset:self.offset+count]

		# In the course of a normal run, hitting EOF unexpectedly is an
		# error, so raise an exception.
		if len(res) < count:
			raise PatchFormatException("Unexpected end-of-file at offset "
					"%d of %r trying to read %d bytes" %
					(self.offset, self.handle.name, count))

		self.crc = crc32(res, self.crc) & 0xFFFFFFFF
		self.offset += len(res)
		return res

	def read_uint8s(self, count):
		"""
		Read an arbitrary number of 8-bit integers from the file.
		"""
		bytes = self.read_bytes(count)
		return array("B", bytes)

	def read_uint8(self):
		"""
		Read an unsigned 8-bit integer from the file.
		"""
		byte = self.read_bytes(1)
		return ord(byte)

	def read_uint32(self):
		"""
		Read a signed 32-bit integer from the file.
		"""
		byte = self.read_bytes(4)
		return unpack("<I", byte)[0]

	def read_var_int(self):
		"""
		Read a variable-length integer from the file.

		I don't understand how this encoding scheme works, but it seems to be
		implemented correctly.
		"""
		res = 0
		shift = 1

		while True:
			byte = self.read_uint8()
			res += (byte & 0x7f) * shift
			if byte & 0x80: break
			shift <<= 7
			res += shift

		return res

	def close(self):
		self.mmap.close()
		self.handle.close()


class CRCTrackingFileWriter(object):
	"""
	File handle that calculates a CRC as bytes are written.

	Also provides methods that are useful for implementing UPS patching.
	"""

	def __init__(self, filename):
		self.handle = open(filename, 'wb')
		self.crc = 0

		# Since we never seek after we've opened the file, we can make things
		# faster by keeping track of the offset ourselves instead of calling
		# .tell()
		self.offset = 0

	def write_bytes(self, bytes):
		self.crc = crc32(bytes, self.crc) & 0xFFFFFFFF
		self.offset += len(bytes)
		self.handle.write(bytes)

	def write_uint8s(self, ints):
		"""
		Write an arbitrary number of 8-bit integers to the file.
		"""
		self.write_bytes(array("B", ints).tostring())

	def close(self):
		self.handle.close()


