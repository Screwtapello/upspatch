"""
Code for reading and applying a UPS patch.
"""
# For copyright and licensing information, see the file COPYING.
from upspatch.exceptions import UPSException, PatchFormatException

UPS_MAGIC = "UPS1"


class UPSPatchApplier(object):
	"""
	A class for applying UPS patches to files.

	Since there's only one public method, this could as well just be one big
	function, but it would be utterly enormous and difficult to follow.
	"""

	def __init__(self, patchHandle, inputHandle, outputHandle):
		# Store our file-handles
		self.patchHandle = patchHandle
		self.inputHandle = inputHandle
		self.outputHandle = outputHandle

		# The offset in the patch file that marks the end of the patch-hunks.
		# There's no marker in the file, so we have to calculate it.
		self.patchHunkEnd = None

		# Once we've figured out how big the output file should be, the answer
		# goes here.
		self.outputSize = None

		# UPS has two file-lengths and two file CRCs. Once we figure which of
		# the two refers to our input file, we'll set this flag.
		self.file1IsInput = None

	def _check_patch_magic(self):
		"""
		Check the patch's magic number is what we expect.
		"""
		actualMagic = self.patchHandle.read_bytes(4)
		if actualMagic != UPS_MAGIC:
			raise PatchFormatException("Patch file had magic %r, not %r" %
					(actualMagic, UPS_MAGIC))

	def _load_file_sizes(self):
		"""
		Read file-sizes from the patch file and guess which is the input.

		Because UPS is a bidirectional patch format, we have to guess which of
		these is the input and which is the output.
		"""
		expectedFile1Size = self.patchHandle.read_var_int()
		expectedFile2Size = self.patchHandle.read_var_int()

		if expectedFile1Size == expectedFile2Size:
			# If the input and output file-sizes are the same, we can't yet
			# determine which of them is file 1 and which of them is file2...
			self.file1IsInput = None

			# ...but we know how big the output file is going to be.
			self.outputSize = expectedFile1Size

			# ...and the input file ought to be the same size.
			if self.inputHandle.size != expectedFile1Size:
				raise UPSException("Expected input file to be %d bytes, got a "
						"%d-byte file instead" %
						(expectedFile1Size, self.inputHandle.size))

		elif expectedFile1Size == self.inputHandle.size:
			# If the input and output file-sizes are different, we know for
			# sure which way around the files go...
			self.file1IsInput = True

			# ...and of course we still know what the output's going to be.
			self.outputSize = expectedFile2Size
		elif expectedFile2Size == self.inputHandle.size:
			self.file1IsInput = False
			self.outputSize = expectedFile1Size
		else:
			# The input file-size doesn't match either of our expected sizes.
			raise UPSException("Expected input file to be %d or %d bytes "
					"long, got a %d-byte file instead" % (expectedFile1Size,
						expectedFile2Size, self.inputHandle.size))

		assert self.outputSize is not None

	def _implicit_copy_if_needed(self):
		"""
		Do one last copy hunk if necessary.

		Every patch-hunk cycle ends with an XOR hunk, but if the end of the
		file requires a copy operation, the patch file just stops with the
		output file shorter than the expected output size, and there's an
		implicit 'copy the rest of the file' hunk.
		"""
		if self.outputHandle.offset < self.outputSize:
			#print "Copying last chunk from patch"
			copySize = self.outputSize - self.outputHandle.offset
			copyBytes = self.inputHandle.read_bytes(copySize)
			self.outputHandle.write_bytes(copyBytes)

	def _copy_hunk(self):
		"""
		First phase of the patch-hunk cycle is copying data verbatim.
		"""
		copySize = self.patchHandle.read_var_int()
		#print "Copying %d raw bytes" % copySize
		if self.inputHandle.offset < self.inputHandle.size:
			copyBytes = self.inputHandle.read_bytes(copySize)
		else:
			copyBytes = "\0" * copySize
		if self.outputHandle.offset < self.outputSize:
			# FIXME: If the output file is less than the target size, but
			# writing copySize bytes would push it over the limit, should
			# we trim the output or raise an exception?
			assert copySize <= (self.outputSize - self.outputHandle.offset)
			self.outputHandle.write_bytes(copyBytes)

	def _xor_hunk(self):
		"""
		Second phase of the patch-hunk cycle is XORing data.
		"""
		# We XOR data from the patch with data from the input file until we get
		# a NUL byte. It would be simpler to process the XOR one byte at
		# a time, but doing things one byte at a time made things 8x slower in
		# my testing.
		xorSize = self.patchHandle.count_bytes_till_null()
		#print "Copying %d XOR bytes" % xorSize
		patchBytes = self.patchHandle.read_uint8s(xorSize)
		assert patchBytes[-1] == 0

		if self.inputHandle.offset + xorSize > self.inputHandle.size:
			# If we reach the end of the input file before we've reached
			# the end of the output file, the rest of the patch is XOR'd
			# with NUL bytes - that is, just copied directly from the
			# patch. This also happens if the last bytes of the input and
			# output files are different, and the patcher needs ot put an
			# extra 'NUL' byte to signal the end of the XOR hunk.
			inputBytesAvailable = self.inputHandle.size - self.inputHandle.offset
			inputBytes = self.inputHandle.read_uint8s(inputBytesAvailable)
			inputBytes.extend([0] * (xorSize - inputBytesAvailable))
		else:
			inputBytes = self.inputHandle.read_uint8s(xorSize)

		outputBytes = [
				inputByte^patchByte
				for inputByte, patchByte
				in zip(inputBytes, patchBytes)
			]

		if self.outputHandle.offset + xorSize > self.outputSize:
			# The last hunk of the patch file is necessarily an XOR hunk,
			# but if the last bytes of the original and patched files
			# aren't identical, there's no terminating NUL byte for the XOR
			# hunk to end with. The patch hunk includes the NUL byte
			# anyway, and relies on the patcher to just not write any bytes
			# past the end of the output file.
			bytesToWrite = self.outputSize - self.outputHandle.offset
			outputBytes = outputBytes[:bytesToWrite]
		self.outputHandle.write_uint8s(outputBytes)

		#print xorSize, len(patchBytes), len(inputBytes), len(outputBytes)

	def _check_CRCs(self):
		"""
		Confirm that the input and output files have the CRCs we expect.
		"""
		file1CRC = self.patchHandle.read_uint32()
		file2CRC = self.patchHandle.read_uint32()

		if self.file1IsInput is None:
			# We couldn't tell whether the input was file1 or file2 from the
			# length alone, but now we can tell for sure.
			if file1CRC == self.inputHandle.crc:
				self.file1IsInput = True
			elif file2CRC == self.inputHandle.crc:
				self.file1IsInput = False

		if self.file1IsInput is True:
			# We already know for sure that file1 is the input file.
			if file1CRC != self.inputHandle.crc:
				raise UPSException("Expected input file to have CRC %#08x, "
						"got %#08x instead" % (file1CRC, self.inputHandle.crc))
			if file2CRC != self.outputHandle.crc:
				raise UPSException("Expected output file to have CRC %#08x, "
						"got %#08x instead" % (file2CRC, self.outputHandle.crc))
		elif self.file1IsInput is False:
			if file1CRC != self.outputHandle.crc:
				raise UPSException("Expected output file to have CRC %#08x, "
						"got %#08x instead" % (file1CRC, self.outputHandle.crc))
			if file2CRC != self.inputHandle.crc:
				raise UPSException("Expected input file to have CRC %#08x, "
						"got %#08x instead" % (file2CRC, self.inputHandle.crc))
		else:
			# We've got no idea what's going on.
			raise UPSException("Expected input file to have CRC %#08x or "
					"%#08x, got a file with CRC %#08x instead" %
					(file1CRC, file2CRC, self.inputHandle.crc))

		# Check the patch CRC, too.
		actualPatchCRC = self.patchHandle.crc
		expectedPatchCRC = self.patchHandle.read_uint32()
		if actualPatchCRC != expectedPatchCRC:
			raise PatchFormatException("Corrupted patch: expected CRC %#08x, "
					"got CRC %#08x instead" %
					(expectedPatchCRC, actualPatchCRC))

	def apply(self):
		"""
		Reads the patch, applies to the input, writes the result to the output.
		"""
		self._check_patch_magic()

		# The only way we know when we're done with the patch-hunks is when we
		# get near the end of the patch file. The last 12 bytes are the CRCs
		# for file1, file2 and the patch itself, so when we get to
		# 12-bytes-from-the-end, then stop.
		self.patchHunkEnd = self.patchHandle.size - 12

		self._load_file_sizes()

		while self.patchHandle.offset < self.patchHunkEnd:
			#print "We've written %d/%d bytes to the output file" % (
			#		outputHandle.offset, outputSize)
			#print "We've read %d/%d bytes of the input file" % (
			#		inputHandle.offset, inputHandle.size)
			#print "We've read %d/%d bytes of patch data" % (
			#		patchHandle.offset, patchHunkEnd)
			self._copy_hunk()
			self._xor_hunk()

		if self.patchHandle.offset > self.patchHunkEnd:
			raise PatchFormatException("Ran into the CRC section")

		self._implicit_copy_if_needed()

		assert self.outputHandle.offset == self.outputSize, ("Output "
				"should be %d bytes, but it's %d" %
				(self.outputSize, self.outputHandle.offset))

		self._check_CRCs()
